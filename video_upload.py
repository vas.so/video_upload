import json
import logging
import os

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait


class VideoOptionsBuilder(object):
    def __init__(self,
                 file_path: str = None,
                 title: str = None,
                 categories: str = None,
                 description: str = None):
        """ Helper class for building video options

        :param file_path: Path to file to upload
        :param title: Title of video to be shown
        :param categories: Video tags. Will be trying to add all, but limited by service, try to give common names
        :param description: Description of video
        """
        self.__file_path = os.path.abspath(file_path) if file_path else ''
        self.__title = title or ''
        self.__categories = categories or []
        self.__description = description or ''

    class VideoOptions(object):
        def __init__(self, file_path: str, title: str, categories: str, description: str):
            """ Object to be build by upper class

            :param file_path: Path to file to upload
            :param title: Title of video to be shown
            :param categories: Video tags. Will be trying to add all, but limited by service, try to give common names
            :param description: Description of video
            """
            self.file_path = file_path
            self.title = title
            self.categories = categories
            self.description = description

    def file_path(self, file_path: str):
        """ Specifying file_path of building object

        :param file_path: Path to file to upload
        :return: current building object
        """
        self.__file_path = file_path
        return self

    def title(self, title):
        """ Specifying title of building object

        :param title: Title of video to be shown
        :return: current building object
        """
        self.__title = title
        return self

    def categories(self, categories: list or tuple):
        """ Specifying categories (tags) of building object

        :param categories: Video tags. Will be trying to add all, but limited by service, try to give common names
        :return: current building object
        """
        self.__categories = categories
        return self

    def description(self, description: str):
        """ Specifying description of building object

        :param description: Description of video
        :return: current building object
        """
        self.__description = description
        return self

    def __fields_not_null(self):
        """ Checks if all field have been provided

        :return: True on success, else - False
        """
        return all([
            self.__description,
            self.__file_path,
            self.__categories,
            self.__title
        ])

    def build(self):
        """ Builds Object with provided values

        :raises: TypeError exception if some of values has not been provided
        :return: built VideoOptions object
        """
        if self.__fields_not_null():
            return self.VideoOptions(self.__file_path, self.__title, self.__categories, self.__description)
        raise TypeError('Some of Video options is incorrect')


class SiteUpload(object):
    def __init__(self,
                 driver: webdriver,
                 site_login_uri: str,
                 site_upload_uri: str,
                 username: str = None,
                 password: str = None):
        """ Helper object to handle with site upload process

        :param driver: Selenium webdriver object
        :param site_login_uri: Url to site login page, or page where login can be found
        :param site_upload_uri: Url to site upload video page, or page where upload can be found
        :param username: User name to login
        :param password: password of User to login
        """
        self.driver = driver
        self.site_login_uri = site_login_uri
        self.site_upload_uri = site_upload_uri
        self.username = username
        self.password = password
        self.video_options = None

    def login(self):
        """ Method for login in to given site with appropriate login fields provided
        """
        self.driver.get(self.site_login_uri)
        search_button, login_button = self.driver.find_elements_by_tag_name('button')
        login_button.click()
        search, username_field, password_field = self.driver.find_elements_by_tag_name('input')
        username_field.send_keys(self.username)
        password_field.send_keys(self.password)
        password_field.submit()

    def redirect(self, uri: str, tries_to_perform: int = 10):
        """ Helper method for be sure to process operations only on needed page

        :param tries_to_perform:
        :param uri: URL of page which must be opened
        """
        self.driver.get(uri)
        counter = 0
        while self.driver.current_url != uri:
            if counter >= tries_to_perform:
                self.driver.get(uri)
                try:
                    WebDriverWait(driver, 100, poll_frequency=10)\
                        .until(expected_conditions.url_to_be(uri))
                except:
                    counter += 1

    def upload(self, file_path, title, description, categories):
        """ Method for handling file upload

        :param file_path: Path to file to upload
        :param title: Title of video to be shown
        :param categories: Video tags. Will be trying to add all, but limited by service, try to give common names
        :param description: Description of video
        """
        self.redirect(self.site_upload_uri)
        self.video_options = (
            VideoOptionsBuilder()
            .file_path(file_path)
            .description(description)
            .categories(categories)
            .title(title)
            .build()
        )
        self.__update_fields()
        logging.getLogger(__name__).info('video has been successfully uploaded')

    def __categories_input(self):
        """ Helper method for handling input of categories (tags)
        """
        categories_field = self.driver.find_element_by_xpath("//input[@id='react-select-6-input']")
        for category in self.video_options.categories:
            categories_field.send_keys(category)
            try:
                wait = WebDriverWait(driver, 10)
                categories_list = (
                    wait.until(
                        expected_conditions
                        .visibility_of_element_located((
                            By.XPATH, '//div[starts-with(@id, "react-select-6-option-")]'))
                    ))
                self.driver.find_element_by_xpath('//div[starts-with(@id, "react-select-6-option-")]')
                categories_list.click()
            except NoSuchElementException as e:
                [categories_field.send_keys(Keys.BACK_SPACE) for i in range(len(category))]
            except TimeoutException as e:
                [categories_field.send_keys(Keys.BACK_SPACE) for i in range(len(category))]

    def __update_fields(self):
        """ Main method of updating video upload fields
        """
        # File
        driver.find_element_by_xpath("//input[@type='file']").send_keys('/home/vasso/1.mp4')
        WebDriverWait(driver, 10).until(
            expected_conditions.visibility_of_element_located((
                By.XPATH, "//div[input/@id='react-select-3-input']")))
        # Orientation
        self.driver.find_element_by_xpath("//div[input/@id='react-select-3-input']").click()
        straight, tranny, gay = [self.driver.find_element_by_id(f'react-select-3-option-{i}') for i in range(3)]
        straight.click()
        # Categories
        self.__categories_input()
        # Title
        self.driver.find_element_by_xpath("//input[@name='title']").send_keys(self.video_options.title)
        # Description
        self.driver.find_element_by_tag_name('textarea').send_keys(self.video_options.description)
        # Production type
        self.driver.find_element_by_xpath('//div[input/@id="react-select-2-input"]').click()
        self.driver.find_element_by_xpath('//div[starts-with(@id, "react-select-2-option-")]').click()
        # Submit
        _, submit_upload_button = self.driver.find_elements_by_xpath('//button[starts-with(@type, "submit")]')
        submit_upload_button.click()


if __name__ == '__main__':
    with open("data.json", "r") as file_path:
        json_data = json.load(file_path)

    login_data = {
        'username': os.environ.get('username') or 'your-string',
        'password': os.environ.get('password') or 'your-string'
    }

    site_login_uri = os.environ.get('site_login_uri') or json_data['site_login_uri']
    site_upload_uri = os.environ.get('site_login_uri') or json_data['site_upload_uri']
    if 'upload_video' in json_data:
        video = json_data['upload_video']
        file_path = video['file_path']
        video_title = video['video_title']
        video_description = video['video_description']
        video_tags = video['video_tags']
    else:
        file_path = os.environ.get('file_path')
        video_title = os.environ.get('video_title')
        video_description = os.environ.get('video_description')
        video_tags = os.environ.get('video_tags')

    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    driver = webdriver.Chrome(options=options)

    try:
        site = SiteUpload(driver, site_login_uri, site_upload_uri, **login_data)
        site.login()
        site.upload(file_path, video_title, video_description, video_tags)
    except:
        pass
    finally:
        driver.close()
